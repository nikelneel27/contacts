// import React from 'react';


// const ContactsInput = () => {

//     return (
//         <div className="ui field container">
//             <h2>Add Contact</h2>
//             <div className="contactInput ui field">
//                 <form >
//                     <label>Name</label>
//                     <input type="text" placeholder="Name" required />
//                     <div className="email">
//                         <label>E-mail</label>
//                         <input type="text" placeholder="E-mail" required />
//                     </div>
//                     <button className="ui button">Submit</button>

//                 </form>
//             </div>
//         </div>

//     );
// }
// export default ContactsInput;

// import React, { Component } from 'react';
// import ContactsDisplay from "./ContactsDisplay"
// class ContactsInput extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             name: '',
//             email: '',
//             datas: []
//         }
//         this.submitContact = this.submitContact.bind(this)
//     }
//     submitContact = () => {
//         console.log("ENTERED DATA:", this.state.name, this.state.email)
//         let obj = { name: this.state.name, email: this.state.email }
//         this.state.datas.push(obj)
//     }
//     render() {
//         return (
//             <>
//             <div className="ui field container">
//                 <h2>Add Contact</h2>
//                 <div className="contactInput ui field">
//                     <div>
//                         <label>Name</label>
//                         <input onChange={(e) => this.setState({ name: e.target.value })} type="text" placeholder="Name" required />
//                         <div className="email">
//                             <label>E-mail</label>
//                             <input onChange={(e) => this.setState({ email: e.target.value })} type="text" placeholder="E-mail" required />
//                         </div>
//                         <button className="ui button" onClick={this.submitContact.bind(this)}>Submit</button>

//                     </div>
//                 </div>
//             </div>
//             <ContactsDisplay contacts={this.state.datas} />
//             </>

//         );
//     }
// }

// export default ContactsInput;

import React, { Component } from 'react';
import ContactsDisplay from './ContactsDisplay';


class ContactsInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contacts: {
                name: "",
                email: ""
            },
            newContacts: { name: "", email: "" }
        };


        const add = (e) => {
            e.preventDefault();
            if (this.state.name === "" || this.state.email === "") {
                alert('All fields are Mandatory');
                return;
            }
            const newContacts = { name: this.state.name, email: this.state.email };
            console.log("New contacts", newContacts);
            this.setState({ newContacts: { name: "", email: "" } });
        }
    }
    render() {
        return (
            <div className="container">
                <h2>Add Contact</h2>
                <div className="contactInput">
                    <form className="form" onSubmit={this.add}>
                        <div className="name">
                            <label>Name</label>
                            <input type="text"
                                placeholder="Enter your Name"
                                required
                                value={this.state.name}
                                onChange={(e) => this.setState({ name: e.target.value })} />
                        </div>
                        <div className="email">
                            <label>Email</label>
                            <input type="text"
                                placeholder="Enter your Email"
                                required
                                value={this.state.email}
                                onChange={(e) => this.setState({ email: e.target.value })} />
                        </div>
                        <button className="Button">Add Contact</button>
                    </form>
                </div>
            </div>


        );
        <ContactsDisplay contacts={contacts} />

    }

}

export default ContactsInput;
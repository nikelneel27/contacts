import React from 'react';
import ContactCard from './ContactCard';

const ContactsDisplay = (props) => {

    const renderContactsList = props.contacts.map((contact) => {
        return (
            <ContactCard contact={contact} />
        );

    });

    return (
        <div className="contactsList">
            <h3>Contacts List</h3>

            <div>
                {renderContactsList}
            </div>
        </div>
    );
};

export default ContactsDisplay;
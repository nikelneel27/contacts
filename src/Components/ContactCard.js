import React from 'react';
import user from '../images/userlogo.svg'

const ContactCard = (props) => {

    const { name, email } = props.contact;
    return (
        <div className="item container">
            <div className="contactDetails">
                <img className="imageUser" src={user} alt="user" width="50px" height="50px" />
                <div className="details">
                    <span>{name}</span>
                    <span>{email}</span>
                </div>
                <div className="del-icon"> <i class="far fa-trash-alt" ></i></div>
            </div>


        </div>

    );
}

export default ContactCard;